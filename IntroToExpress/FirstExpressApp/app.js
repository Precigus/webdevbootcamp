var express = require("express");
var app = express();

// "/" -> "Hi there"
app.get("/", function(req, res){
    res.send("Hi there!");
});

// "/bye" -> "Goodbye"
app.get("/bye", function(req, res){
    res.send("Bye!");
});

// "/dog" -> "Meow"
app.get("/dog", function(req,res){
    res.send("MEOW!");
});

app.get("/r/:subredditName", function(req, res){
    var subreddit = (req.params.subredditName);
    res.send("WELCOME TO THE " + subreddit.toUpperCase() + " SUBREDDIT");
});

app.get("/r/:subredditName/comments/:id/:title", function(req, res){
    console.log(req.params);
    res.send("Welcome to the comments page");
});

// "*" runs if an undefined route is callled
app.get("*", function(req,res){
    res.send("YOU ARE A STAR");
});

// Tell the app to listen for requests (start server)
app.listen(3000, function(){
    console.log("Server started on port 3000");
});