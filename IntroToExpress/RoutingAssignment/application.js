var express = require("express");
var app = express();

app.get("/", function (req, res) {
    res.send("Hi there, welcome to my assignment");
});

app.get("/speak/:animal", function (req, res) {
    var sounds = {
        pig: "OINK",
        cow: "MOOO",
        dog: "WOOF",
        cat: "MEOW",
        fox: "\"What does it say exactly\""
    }

    var animal = req.params.animal.toLowerCase();
    var sound = sounds[animal];
    
    res.send("The " + animal + " says: " + sound);
})

app.get("/repeat/:word/:count", function (req, res) {
    var word = req.params.word;
    var num = Number(req.params.count);
    var string = "";
    for (var i = 0; i < num; i++) {
        string += word + " ";
    }
    res.send(string);
})

app.get("*", function (req, res) {
    res.send("Sorry, page not found... What are you doing with your life?");
});

app.listen(4001, function () {
    console.log("Server for routing asignment started on port 4001");
});