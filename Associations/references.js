const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/blog_demo_2", { useNewUrlParser: true });

var Post = require("./models/post");
var User = require("./models/user");

Post.create({
    title: "What to do with CLU",
    content: "Keep an eye on him"
}, function (err, post) {
    User.findOne({ email: "kevin@encom.edu" }, function (err, foundUser) {
        if (err) {
            console.log(err);
        } else {
            foundUser.posts.push(post);
            foundUser.save(function(err, data){
                console.log(err ? err : data);
            });
        }
    });
});

// Find user
// Find all the posts for that user

// User.findOne({email:"kevin@encom.edu"}).populate("posts").exec(function(err, user){
//     console.log(err ? err : user);
// });
