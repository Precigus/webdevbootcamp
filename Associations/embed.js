const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/blog_demo", { useNewUrlParser: true });

// Post - title, content
var postSchema = new mongoose.Schema({
    title: String,
    content: String
});

var Post = mongoose.model("Post", postSchema);

// User - email, name
var userSchema = new mongoose.Schema({
    email: String,
    name: String,
    posts: [postSchema]
});

var User = mongoose.model("User", userSchema);

// var newUser = new User({
//     email: "Sam@Encom.edu",
//     name: "Sam Flynn"
// });

// newUser.posts.push({
//     title: "How to enter the Grid",
//     content: "Try using the console in the basement of the arcade"
// });

// newUser.save(function (err, user) {
//     console.log(err ? err : user);
// });

// var newPost = new Post({
//     title: "Reflections on apples",
//     content: "They are delicious"
// });

// newPost.save(function (err, post) {
//     console.log(err ? err : post);
// });

User.findOne({ name: "Sam Flynn" }, function (err, user) {
    if (err) {
        console.log(err);
    } else {
        user.posts.push({
            title: "Grid survival guide",
            content: "Lesson one. Don't run from the programs"
        });
        user.save(function(err, user){
            console.log(err ? err : user);
        });
    }
});