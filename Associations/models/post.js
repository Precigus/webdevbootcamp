const mongoose = require("mongoose");

// Post - title, content
var postSchema = new mongoose.Schema({
    title: String,
    content: String
});

// We have to declare what we want returend from the file:
module.exports = mongoose.model("Post", postSchema);