var mongoose = require("mongoose");
// User - email, name
var userSchema = new mongoose.Schema({
    email: String,
    name: String,
    posts: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Post"
        }
    ]
});

// We have to declare what we want returend from the file:
module.exports = mongoose.model("User", userSchema);