var express = require("express");
var app = express();

app.use(express.static("public"));
app.set("view engine", "ejs");

app.get("/", function(req,res){
    res.render("home");
})

app.get("/grid/:thing", function(req, res){
    var thing = req.params.thing;
    res.render("grid", {thingVar: thing});
})

app.get("/isos", function (req,res){
    var isos = [
            {name: "Quarra", gender: "Female"},
            {name: "Giles", gender: "Male"},
            {name: "Ophellia", gender: "Female"},
    ];

    res.render("isos", {isos: isos});
});

app.get("*", function(req, res){
    res.send("Page does not exist");
})

app.listen(4002, function () {
    console.log("Server for ejs exercise started on port 4002");
});