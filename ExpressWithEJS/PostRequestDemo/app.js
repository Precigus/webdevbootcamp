var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: true}));

app.set("view engine", "ejs");

var friends = ["Tony", "Steve", "Natasha", "Bruce", "Steven"];

app.get("/", function (req, res) {
    res.render("home");
});

app.get("/friends", function (req, res) {
    res.render("friends", {
        friends: friends
    });
})

app.post("/addfriend", function (req, res) {
    var newFriend = req.body.newFriend;
    friends.push(newFriend);
    res.redirect("/friends");
});

app.get("*", function (req, res) {
    res.send("This page does not exist");
})

app.listen("4003", function () {
    console.log("App server for Post request demo started on port 4003");
});