const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/Programs_app", { useNewUrlParser: true });

const programSchema = new mongoose.Schema({
    name: String,
    purpose: String,
    color: String,
    presumed_age: Number
});

const Program = mongoose.model("Program", programSchema);

//add a new cat to the database

// var tron = new Program({
//     name: "Clue",
//     purpose: "Building the perfect system",
//     color: "Bright orange",
//     presumed_age: 6
// });

// tron.save(function(err, program){
//     if (err){
//         console.log("something went wrong");
//     } else{
//         console.log("Program saved to DB");
//         console.log(program);
//     }
// });

Program.create({
    name: "Castor",
    purpose: "Bar owner/dealer",
    color: "White",
    presumed_age: 4
}, function(err, program){
    if (err){
        console.log("Error occured!");
        console.log(err);
    } else {
        console.log("Program created: ");
        console.log(program);
    }
});

//retrieve all cats from the database and console.log each of them

Program.find({}, function(err, programs){
    if (err){
        console.log("An error occured!");
        console.log(err);
    } else {
        console.log("All the available programs");
        console.log(programs);
    }
})