RESTFUL ROUTES (so far)

Nr  Name    URL                             Verb    Description
=========================================================================
1.  INDEX   /campgrounds                    GET     Display a list of all campgrounds

2.  NEW     /campgrounds/new                GET     Displays form to create a new campgound

3.  CREATE  /campgrounds                    POST    Add a new campground to DB

4.  SHOW    /campgrounds/:id                GET     Shows info about one campground

5.  EDIT    /campgrounds/:id/edit           GET     Shows the edit form for one campground

6.  UPDATE  /campgrounds/:id                PUT     Updates a particular campground, then redirects somewhere

7.  DESTROY /campgrounds/:id                DELETE  Deletes a particular campground, then redirects somewhere

8.  NEW     /campgrounds/:id/comments/new   GET

9.  CREATE  /campgrounds/:id/comments       POST