const express = require("express"),
  router = express.Router(),
  passport = require("passport"),
  User = require("../models/user"),
  Campground = require("../models/campground"),
  async = require("async"),
    nodemailer = require("nodemailer"),
    crypto = require("crypto"),
    Notification = require("../models/notification"), 
    {isLoggedIn} = require("../middleware"),
    {getUrl} = require("../middleware");


// ROOT route
router.get("/", function (req, res) {
  res.render("landing");
});

// show registration form
router.get("/register", function (req, res) {
  res.render("register", {
    page: "register"
  });
});

// handle Sign Up process
router.post("/register", function (req, res) {
  var newUser = new User({
    username: req.body.username,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    avatar: req.body.avatar
  });

  if (req.body.adminCode === process.env.AdminCode) {
    newUser.isAdmin = true;
  }

  User.register(newUser, req.body.password, function (err, user) {
    if (err) {
      console.log(err);
      req.flash("error", err.message);
      return res.render("register", {
        error: err.message
      });
    }
    passport.authenticate("local")(req, res, function () {
      req.flash("success", "Welcome to Yelpcamp, " + user.username);
      res.redirect("/campgrounds");
    });
  });
});

// show Login form
router.get("/login", getUrl, function (req, res) {
  res.render("login", {
    page: "login"
  });
});

// Handle Login logic
router.post(
  "/login",
  passport.authenticate("local", {
    successReturnToOrRedirect: "/campgrounds",
    failureRedirect: "/login"
  })
);

// Logout route
router.get("/logout", function (req, res) {
  var backUrl = req.header("Referer");
  req.logout();
  req.flash("success", "Logged you out");
  res.redirect(backUrl);
});

router.get("/forgot", function (req, res) {
  res.render("forgot");
});

router.post("/forgot", function (req, res, next) {
  async.waterfall(
    [
      function (done) {
        crypto.randomBytes(20, function (err, buf) {
          var token = buf.toString("hex");
          done(err, token);
        });
      },
      function (token, done) {
        User.findOne({
            email: req.body.email
          },
          function (err, user) {
            if (!user) {
              req.flash(
                "error",
                "There is no registered user associated with this email address in the system"
              );
              return res.redirect("/forgot");
            }

            user.resetPasswordToken = token;
            user.resetPasswordExpires = Date.now() + 3600000; //1 hour in miliseconds

            user.save(function (err) {
              done(err, token, user);
            });
          }
        );
      },
      function (token, user, done) {
        var smtpTransport = nodemailer.createTransport({
          service: "Gmail",
          auth: {
            user: "jusgrekinfo@gmail.com",
            pass: process.env.GmailPW
          }
        });
        var mailOptions = {
          to: user.email,
          from: "jusgrekinfo@gmail.com",
          subject: "Password reset for YelpCamp",
          text: "You are receiving this email because a password change for the account associated with it has been requested.\n\n" +
            "Please click the following link, or paste it in your browser to complete the process:\n\n" +
            "https://" +
            req.headers.host +
            "/reset" +
            token +
            "\n\n" +
            "If you did not request a password change, ignore this email. Your password will remain unchanged.\n"
        };
        smtpTransport.sendMail(mailOptions, function (err) {
          console.log("Password reset email sent");
          req.flash(
            "success",
            "Please check your email address for further instructions regarding password change."
          );
          done(err, "done");
        });
      }
    ],
    function (err) {
      if (err) return next(err);
      res.redirect("/forgot");
    }
  );
});

router.get("/reset/:token", function (req, res) {
  User.findOne({
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    },
    function (err, user) {
      if (!user) {
        req.flash("error", "Password reset token has expired");
        return res.redirect("/forgot");
      }
      res.render("reset", {
        token: req.params.token
      });
    }
  );
});

router.post("/reset/:token", function (req, res) {
  async.waterfall(
    [
      function (done) {
        User.FindOne({
            resetPasswordToken: req.params.token,
            resetPasswordExpires: {
              $gt: Date.now()
            }
          },
          function (err, user) {
            if (!user) {
              req.flash("error", "password reset token is invalid/expired.");
              return res.redirect("back");
            }
            if (req.body.password === req.body.confirm) {
              user.setPassword(req.body.password, function (err) {
                //setPassword is a passport-local-mongoose method
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;

                user.save(function (err) {
                  req.logIn(user, function (err) {
                    done(err, user);
                  });
                });
              });
            } else {
              req.flash("error", "Password do not match.");
              return res.redirect("back");
            }
          }
        );
      },
      function (user, err) {
        var smtpTransport = nodemailer.createTransport({
          service: "Gmail",
          auth: {
            user: "jusgrekinfo@gmail.com",
            pass: process.env.GmailPW
          }
        });
        var mailOptions = {
          to: user.email,
          from: "jusgrekinfo@gmail.com",
          subject: "Your YelpCamp password has been changed.",
          text: "Hello, " +
            user.firstName +
            " " +
            user.lastName +
            "\n\n" +
            "This is a confirmation that the password for your account " +
            user.username +
            " has been changed succesfully."
        };
        smtpTransport.sendMail(mailOptions, function (err) {
          req.flash("success", "");
          done(err);
        });
      }
    ],
    function (err) {
      if (err) return next(err);
      res.redirect("/campgrounds");
    }
  );
});

// User profile
router.get("/users/:id", async function (req, res) {
  try {
    let user = await User.findById(req.params.id).populate("followers").exec();
    let campgrounds = await Campground.find()
      .where("author.id")
      .equals(user._id)
      .exec();
    res.render("users/show", {
      user,
      campgrounds
    });
  } catch (err) {
    req.flash("error", "Something went wrong");
    req.redirect("/");
  }
});

// Follow user
router.get("/follow/:id", isLoggedIn, async function (req, res) {
  try {
    let user = await User.findById(req.params.id);
    user.followers.push(req.user._id);
    user.save();
    req.flash("success", "Successfully followed " + user.username + "!");
    res.redirect("/users/" + req.params.id);
  } catch (err) {
    req.flash("error", err.message);
    return res.redirect("back");
  }
});

// View all notifications
router.get("/notifications", isLoggedIn, async function (req, res) {
  try {
    let user = await User.findById(req.user._id).populate({
      path: "notifications",
      options: {
        sort: {
          _id: -1
        }
      }
    }).exec();
    let allNotifications = user.notifications;
    res.render("notifications/index", {
      allNotifications
    });
  } catch (err) {
    req.flash("error", err.message);
    releaseEvents.redirect("back");
  }
});

// Handle notifications
router.get("/notifications/:id", isLoggedIn, async function (req, res) {
  try {
    let notification = await Notification.findById(req.params.id);
    notification.isRead = true;
    notification.save();
    res.redirect("/campgrounds/" + notification.campgroundId);
  } catch (err) {
    req.flash("error", err.message);
    res.redirect("back");
  }
});

// Test code
function createMailer() {
  var smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
      user: "jusgrekinfor@gmail.com",
      pass: process.env.GmailPW
    }
  });
}

module.exports = router;