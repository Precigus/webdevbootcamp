var mongoose = require("mongoose");

var campgroundSchema = new mongoose.Schema({
  name: String,
  image: {
    imageId: String,
    url: String,
  },
  price: Number,
  description: String,
  location: String,
  lat: Number,
  lng: Number,
  createdAt: {
    type: Date,
    default: Date.now
  },
  author: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    username: String
  },
  comments: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Comment"
  }],
  reviews: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Review"
  }],
  rating: {
    type: Number,
    default: 0
  }
} 
// {
//   usePushEach: true
// }
);

module.exports = mongoose.model("Campground", campgroundSchema);